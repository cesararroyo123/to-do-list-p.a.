package cesar.todolistpa;

public interface ActivityCallback {
    void onPostSelected(int pos);
}
